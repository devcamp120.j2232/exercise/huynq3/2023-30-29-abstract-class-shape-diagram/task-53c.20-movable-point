import interfaces.Movable;
import models.MovablePoint;
public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        MovablePoint point1= new MovablePoint(5,6);
        MovablePoint point2= new MovablePoint(5,6);
        System.out.println(point1);
        System.out.println(point2);
//di chuyển
        point1.moveDown();
        point1.moveDown();
        point1.moveRight();
        System.out.println(point1);

        point2.moveUp();
        point2.moveUp();
        point2.moveLeft();
        System.out.println(point2);


    }
}
