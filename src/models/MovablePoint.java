package models;
import interfaces.Movable;

public class MovablePoint implements Movable {
    private int x;
    private int y;
    public MovablePoint(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    @Override
    public void moveDown() {
        // TODO Auto-generated method stub
        this.y++;
        

    }
    @Override
    public void moveLeft() {
        // TODO Auto-generated method stub
        this.y--;
    }
    @Override
    public void moveRight() {
        // TODO Auto-generated method stub
        this.x--;
    }
    @Override
    public void moveUp() {
        // TODO Auto-generated method stub
        this.x++;
    }

    @Override
    public String toString() {
        return "MovablePoint [x=" + x + ", y=" + y + "]";
    }

    


    
}
